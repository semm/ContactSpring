/**
 * 
 */
package com.contact.springboot.gradle.contact.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.contact.springboot.gradle.contact.entities.Employee;
import com.contact.springboot.gradle.contact.entities.Person;
import com.contact.springboot.gradle.contact.service.ContactsService;

/**
 * @author Android
 *
 */
@RestController
@RequestMapping("/rest/api/employer")
public class ContactsController {
	
	@Autowired
	public ContactsService contactService;
	
	@GetMapping("/status")
	public String status() {
		String str = "OK !!!" ;
		return str;
	}
	
	@GetMapping("/table/{tableName}")
	public List<Employee> getTable(@PathVariable("tableName") String table) {
		String str = "hi there";//contactService.add("add") ? "OK !!!" : "Not Ok ...";
		return contactService.findAll(table);
	}
}

