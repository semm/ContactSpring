/**
 * 
 */
package com.contact.springboot.gradle.contact.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author Android
 *
 */
public class Person {

	String name;
	int id;
	
	

	public Person() {}


	public Person(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "Person [name=" + name + "]";
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

}
