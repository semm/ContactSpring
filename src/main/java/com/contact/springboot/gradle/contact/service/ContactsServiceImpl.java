/**
 * 
 */
package com.contact.springboot.gradle.contact.service;

import java.util.List;

import org.apache.catalina.startup.ClassLoaderFactory.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.contact.springboot.gradle.contact.entities.Employee;
import com.contact.springboot.gradle.contact.entities.Person;
import com.contact.springboot.gradle.contact.repository.ContactsRepository;

/**
 * @author Android
 *
 */
@Service
public class ContactsServiceImpl implements ContactsService {

	@Autowired
	private ContactsRepository repository;
	/* (non-Javadoc)
	 * @see com.contact.springboot.gradle.contact.service.ContactsService#add(java.lang.String)
	 */
	@Override
	public boolean add(String name) {
		return repository.insert(name);
	}

	/* (non-Javadoc)
	 * @see com.contact.springboot.gradle.contact.service.ContactsService#findAll(java.lang.String)
	 */
	@Override
	public List<Employee> findAll(String table) {
		return repository.findAll(table);
	}

}
