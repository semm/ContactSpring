/**
 * 
 */
package com.contact.springboot.gradle.contact.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.contact.springboot.gradle.contact.entities.Employee;
import com.contact.springboot.gradle.contact.entities.Person;
import com.contact.springboot.gradle.contact.entities.PersonRowMapper;

/**
 * @author Android
 *
 */
@Repository
public class ContactsRepositoryImpl implements ContactsRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public String findById(int personId) {
		String sql = "select * from workers where id = ?";
		jdbcTemplate.query(sql, new Object[] {personId}, new PersonRowMapper());
		return null;
	}

	@Override
	public boolean insert(String name) {
		int updated = jdbcTemplate.update("insert into workers (name) values (?)", name);
		return updated > 0 ;
	}
	
	public List<Employee> findAll(String table){
		
		String sql = "SELECT * FROM " + table;
			
		List<Employee> Employees  = jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<Employee>(Employee.class));
			
		return Employees;
	}
}