/**
 * 
 */
package com.contact.springboot.gradle.contact.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.contact.springboot.gradle.contact.entities.Employee;
import com.contact.springboot.gradle.contact.entities.Person;

/**
 * @author Android
 *
 */
public interface ContactsService {
	boolean add(String name);
	
	List<Employee> findAll(String table);
}
