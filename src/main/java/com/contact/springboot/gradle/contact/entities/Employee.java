/**
 * 
 */
package com.contact.springboot.gradle.contact.entities;

/**
 * @author Android
 *
 */
public class Employee {
		int EmployeeID;
		String LastName;
		String FirstName;
		String Title;
		String TitleOfCourtesy;
		String BirthDate;
		String HireDate;
		String Address;
		String City;
		String Region;
		String PostalCode;
		String Country;
		String HomePhone;
		String Extension;
		String Photo;
		String Notes;
		String ReportsTo;
		String PhotoPath;
		String Salary;
		
		/**
		 * @return the employeeID
		 */
		public int getEmployeeID() {
			return EmployeeID;
		}
		/**
		 * @param employeeID the employeeID to set
		 */
		public void setEmployeeID(int employeeID) {
			EmployeeID = employeeID;
		}
		/**
		 * @return the lastName
		 */
		public String getLastName() {
			return LastName;
		}
		/**
		 * @param lastName the lastName to set
		 */
		public void setLastName(String lastName) {
			LastName = lastName;
		}
		/**
		 * @return the firstName
		 */
		public String getFirstName() {
			return FirstName;
		}
		/**
		 * @param firstName the firstName to set
		 */
		public void setFirstName(String firstName) {
			FirstName = firstName;
		}
		/**
		 * @return the title
		 */
		public String getTitle() {
			return Title;
		}
		/**
		 * @param title the title to set
		 */
		public void setTitle(String title) {
			Title = title;
		}
		/**
		 * @return the titleOfCourtesy
		 */
		public String getTitleOfCourtesy() {
			return TitleOfCourtesy;
		}
		/**
		 * @param titleOfCourtesy the titleOfCourtesy to set
		 */
		public void setTitleOfCourtesy(String titleOfCourtesy) {
			TitleOfCourtesy = titleOfCourtesy;
		}
		/**
		 * @return the birthDate
		 */
		public String getBirthDate() {
			return BirthDate;
		}
		/**
		 * @param birthDate the birthDate to set
		 */
		public void setBirthDate(String birthDate) {
			BirthDate = birthDate;
		}
		/**
		 * @return the hireDate
		 */
		public String getHireDate() {
			return HireDate;
		}
		/**
		 * @param hireDate the hireDate to set
		 */
		public void setHireDate(String hireDate) {
			HireDate = hireDate;
		}
		/**
		 * @return the address
		 */
		public String getAddress() {
			return Address;
		}
		/**
		 * @param address the address to set
		 */
		public void setAddress(String address) {
			Address = address;
		}
		/**
		 * @return the city
		 */
		public String getCity() {
			return City;
		}
		/**
		 * @param city the city to set
		 */
		public void setCity(String city) {
			City = city;
		}
		/**
		 * @return the region
		 */
		public String getRegion() {
			return Region;
		}
		/**
		 * @param region the region to set
		 */
		public void setRegion(String region) {
			Region = region;
		}
		/**
		 * @return the postalCode
		 */
		public String getPostalCode() {
			return PostalCode;
		}
		/**
		 * @param postalCode the postalCode to set
		 */
		public void setPostalCode(String postalCode) {
			PostalCode = postalCode;
		}
		/**
		 * @return the country
		 */
		public String getCountry() {
			return Country;
		}
		/**
		 * @param country the country to set
		 */
		public void setCountry(String country) {
			Country = country;
		}
		/**
		 * @return the homePhone
		 */
		public String getHomePhone() {
			return HomePhone;
		}
		/**
		 * @param homePhone the homePhone to set
		 */
		public void setHomePhone(String homePhone) {
			HomePhone = homePhone;
		}
		/**
		 * @return the extension
		 */
		public String getExtension() {
			return Extension;
		}
		/**
		 * @param extension the extension to set
		 */
		public void setExtension(String extension) {
			Extension = extension;
		}
		/**
		 * @return the photo
		 */
		public String getPhoto() {
			return Photo;
		}
		/**
		 * @param photo the photo to set
		 */
		public void setPhoto(String photo) {
			Photo = photo;
		}
		/**
		 * @return the notes
		 */
		public String getNotes() {
			return Notes;
		}
		/**
		 * @param notes the notes to set
		 */
		public void setNotes(String notes) {
			Notes = notes;
		}
		/**
		 * @return the reportsTo
		 */
		public String getReportsTo() {
			return ReportsTo;
		}
		/**
		 * @param reportsTo the reportsTo to set
		 */
		public void setReportsTo(String reportsTo) {
			ReportsTo = reportsTo;
		}
		/**
		 * @return the photoPath
		 */
		public String getPhotoPath() {
			return PhotoPath;
		}
		/**
		 * @param photoPath the photoPath to set
		 */
		public void setPhotoPath(String photoPath) {
			PhotoPath = photoPath;
		}
		/**
		 * @return the salary
		 */
		public String getSalary() {
			return Salary;
		}
		/**
		 * @param salary the salary to set
		 */
		public void setSalary(String salary) {
			Salary = salary;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Employee [EmployeeID=" + EmployeeID + ", LastName=" + LastName + ", FirstName=" + FirstName
					+ ", Title=" + Title + ", TitleOfCourtesy=" + TitleOfCourtesy + ", BirthDate=" + BirthDate
					+ ", HireDate=" + HireDate + ", Address=" + Address + ", City=" + City + ", Region=" + Region
					+ ", PostalCode=" + PostalCode + ", Country=" + Country + ", HomePhone=" + HomePhone
					+ ", Extension=" + Extension + ", Photo=" + Photo + ", Notes=" + Notes + ", ReportsTo=" + ReportsTo
					+ ", PhotoPath=" + PhotoPath + ", Salary=" + Salary + "]";
		}
		
		
}
