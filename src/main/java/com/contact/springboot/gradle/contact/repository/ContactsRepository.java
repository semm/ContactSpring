/**
 * 
 */
package com.contact.springboot.gradle.contact.repository;

import java.util.List;

import com.contact.springboot.gradle.contact.entities.Employee;
import com.contact.springboot.gradle.contact.entities.Person;

/**
 * @author Android
 *
 */
public interface ContactsRepository {
	
	String findById(int personId);
	
	public List<Employee> findAll(String table);
	
	boolean insert(String name);
}
